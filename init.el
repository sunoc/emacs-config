;;; init.el --- Emacs distribution configuration bootstrap
;;; Commentary:
;;; A single .el file that calls the minimum configuration
;;; to load the rest from the README.org


;;; Code:
;;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; Checking Emacs's version beforehand. Need a newer one.
(let ((minver "29.1"))
  (when (version< emacs-version minver)
    (error
     "Your Emacs is too old -- this config requires v%s or higher" minver)))

;;; Packages managers and startup options
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(unless (package-installed-p 'use-package) ;; install use-package
  (package-refresh-contents)
  (package-install 'use-package))

;;; Alaws ensure and defer, reduce the configuration in the README
(setq use-package-always-ensure t)
(setq use-package-always-defer t)

(eval-when-compile
  (require 'use-package))

;;; Fire up package.el
(setq package-enable-at-startup nil)
(package-initialize)

;; Minimize garbage collection during startup
(setq gc-cons-threshold most-positive-fixnum)
;; Lower threshold back to 8 MiB (default is 800kB)
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (expt 2 23))))
(setq read-process-output-max (* 1024 1024)) ;; 1MB


;;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; load files from the lisp directory
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; load most of the configuration from the README itself !
(use-package org
  :ensure t
  :defer nil)
(org-babel-load-file
 (expand-file-name "README.org"
                   user-emacs-directory))


;;; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;;; file used to store the custom variables,
;; including the installed packages names
(setq custom-file "~/.emacs.d/custom.el")
; if the custom-file doesn't already exist, creat it
(unless (file-exists-p custom-file)
   (write-region "" nil custom-file))
(load custom-file)
;;; init.el ends here
